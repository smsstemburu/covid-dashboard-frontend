import React, { Component } from "react";
import NavBar from "./Home/Navbar/Navbar";
import "./App.css";

import Home from "./Home/Home";

class App extends Component {
  render() {
    return (
      <div id="App">
        <NavBar />

        <Home />
        {/* <Containers /> */}
      </div>
    );
  }
}

export default App;
