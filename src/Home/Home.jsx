import React, { Component } from "react";
import "../Styles/Home.css";
import AllCountryStateContainer from "./Containers/AllCountryStateContainer";
import HeadlinesContainer from "./Containers/HeadlinesContainer";
import SpecificCountryContainer from "./Containers/SpecificCountryContainer";
import SpecificStateContainer from "./Containers/SpecificStateContainer";
import Loader from "../loader";

class Home extends Component {
  state = { apiResponse: [], loading: true };
  // callbackFunction = (childData) => {
  //   console.log(childData);
  //   this.setState({ loading: childData });
  // };

  callAPI() {
    fetch("http://localhost:9000/covidHeadlines")
      .then((res) => res.json())
      .then((res) => {
        this.setState({ apiResponse: res, loading: false });
        // this.sendData(false);
      });
  }

  componentWillMount() {
    this.callAPI();
  }
  render() {
    // console.log(this.state.loading);
    if (this.state.loading)
      return (
        <Loader
          style={{
            margin: "0 auto",
            color: "antiquewhite",
          }}
        />
      );
    return (
      <div class="grid-container">
        <div class="grid-item item1">
          <AllCountryStateContainer />
        </div>
        <div class="grid-item item2">
          <HeadlinesContainer
            dataResponse={this.state.apiResponse}
            // dataFromParent={this.state.message}
            // parentCallback={this.callbackFunction}
          />
        </div>
        <div class="grid-item item3">
          <SpecificCountryContainer parentCallback={this.callbackFunction} />
        </div>
        <div class="grid-item item4">
          <SpecificStateContainer />
        </div>
        {/* <p> {this.state.message} </p> */}
      </div>
    );
  }
}

export default Home;
