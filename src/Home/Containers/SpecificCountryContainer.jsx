import React, { Component } from "react";
import { BarChart, Bar, XAxis, YAxis, CartesianGrid, Legend } from "recharts";
import "../../Styles/SpecificCountryContainer.css";
import Loader from "../../loader";

export default class Example extends Component {
  constructor(props) {
    super(props);

    this.state = {
      apiResponse: [],
      countries: [],
      states: [],
      countryValue: "India",
      stateValue: "Telangana",
      result: {},
      countryResult: [
        {
          Country: "Afghanistan",
          NewConfirmed: 30,
          TotalConfirmed: 156196,
          NewDeaths: 1,
          TotalDeaths: 7269,
          NewRecovered: 0,
          TotalRecovered: 0,
        },
      ],
      loading: true,
    };
  }
  static demoUrl =
    "https://codesandbox.io/s/pie-chart-with-customized-label-dlhhj";

  callSpecificCountryAPI() {
    fetch("http://localhost:9000/covidAllCountryStatus")
      .then((res) => res.json())
      .then((res) => this.setState({ countries: res }));
  }

  componentWillMount() {
    this.callSpecificCountryAPI();
  }

  empty(p) {
    //empty your array
    this.setState({ countryResult: [] });
    this.setState((prevState) => ({
      countryResult: [p, ...prevState.countryResult],
    }));
  }

  click2 = async (countryName) => {
    this.setState({ countryValue: countryName, loading: "false" });
    // const stateValue = this.state.stateValue;
    await fetch("http://localhost:9000/covidSpecificCountryStatus", {
      method: "POST",
      headers: { "Content-Type": "application/json" },
      body: JSON.stringify({
        Country: countryName,
      }),
    })
      .then((res) => res.json())
      //   .then((p) => this.setState({countryResult: p}))
      .then((p) => this.empty(p));
    return "success";
  };
  sendData = () => {
    this.props.parentCallback(this.state.loading);
  };
  render() {
    console.log(this.state.countryResult);
    const { countries, states } = this.state;
    let countriesList =
      countries.length > 0 &&
      countries.map((item, i) => {
        return (
          <option key={i} value={item.id}>
            {item.Country}
          </option>
        );
      }, this);

    return (
      <div>
        <h4 className="descriptionBoxHeading">
          <select
            className="specificCountryDropdown"
            onChange={((e) => this.click2(e.target.value), this.sendData)}
          >
            {countriesList}
          </select>
        </h4>
        <div style={{ position: "relative" }}>
          <BarChart
            //   width={((this.state.width-60)>(50*this.state.filteredData.length*this.state.selectedMeasures.length))?(this.state.width-60):(50*this.state.filteredData.length*this.state.selectedMeasures.length)}

            data={this.state.countryResult}
            width={400}
            height={200}
            margin={{ top: 5, right: 30, left: 20, bottom: 5 }}
            style={{
              overflowX: "auto",
              overflowY: "hidden",
              maxHeight: 186,
              maxWidth: 700,

              position: "relative",
              zIndex: "0",
            }}
            barCategoryGap={10}
          >
            <Legend />
            <YAxis />
            <XAxis
              minTickGap={1}
              dataKey="Country"
              style={{ fontSize: "10px", color: "antiquewhite" }}
            />

            <Bar dataKey="NewConfirmed" fill="#EF7B45" />

            <Bar dataKey="NewDeaths" fill="antiquewhite" />

            <Bar dataKey="NewRecovered" fill="#F5C0C0" />
          </BarChart>
        </div>
      </div>
    );
  }
}
