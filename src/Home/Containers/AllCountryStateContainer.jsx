import React, { Component } from "react";
import "../../Styles/AllCountryStateContainer.css";
import { BarChart, Bar, XAxis, YAxis, CartesianGrid, Legend } from "recharts";

const data = [
  {
    Province: "Telangana",
    Confirmed: 670453,
    Deaths: 3949,
    Recovered: 0,
    Active: 666504,
  },
];

class AllCountryStateContainer extends Component {
  constructor(props) {
    super(props);

    this.state = {
      apiResponse: [],
      countries: [],
      states: [],
      countryValue: "India",
      stateValue: "Telangana",
      result: {},
      countryResult: {},
      loading: true,
      selectList: ["Countries", "States"],
      type: "Countries",
    };
  }

  callSpecificStateAPI() {
    fetch("http://localhost:9000/covidAllStateStatus")
      .then((res) => res.json())
      .then((res) => this.setState({ states: res }));
  }
  callSpecificCountryAPI() {
    fetch("http://localhost:9000/covidAllCountryStatus")
      .then((res) => res.json())
      .then((res) => this.setState({ countries: res }));
  }

  componentWillMount() {
    this.callSpecificCountryAPI();
    this.callSpecificStateAPI();
  }

  click2 = (val) => {
    this.setState({ type: val });
  };

  render() {
    const { countries, states, selectList } = this.state;
    let val = this.state.type == "Countries" ? "Country" : "Province";
    let barVal1 = this.state.type == "Countries" ? "NewConfirmed" : "Confirmed";
    let barVal2 = this.state.type == "Countries" ? "NewDeaths" : "Active";
    let List =
      selectList.length > 0 &&
      selectList.map((item, i) => {
        return (
          <option key={i} value={item.id}>
            {item}
          </option>
        );
      }, this);
    return (
      <div>
        <h4 className="descriptionBoxHeading">
          <select
            className="specificCountryDropdown"
            onChange={
              (e) => this.click2(e.target.value)
              // this.sendData
            }
          >
            {List}
          </select>
        </h4>
        <div style={{ position: "relative", width: "100%" }}>
          <BarChart
            data={this.state.type == "Countries" ? countries : states}
            width={this.state.type == "Countries" ? 6000000 : 10000}
            height={200}
            margin={{ top: 5, right: 30, left: 20, bottom: 5 }}
            style={{
              overflowX: "auto",
              overflowY: "hidden",
              maxHeight: 300,
              maxWidth: 850,
              position: "relative",
              zIndex: "0",
            }}
            barCategoryGap={5}
          >
            <XAxis
              minTickGap={1}
              dataKey={val}
              style={{ fontSize: "10px", color: "antiquewhite" }}
            />

            <Legend />
            <Bar dataKey={barVal1} fill="#EF7B45" />

            <Bar dataKey={barVal2} fill="antiquewhite" />
          </BarChart>
        </div>
      </div>
    );
  }
}

export default AllCountryStateContainer;
