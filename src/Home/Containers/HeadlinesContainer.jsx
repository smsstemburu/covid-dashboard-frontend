import React, { Component } from "react";
import "../../Styles/HeadlinesContainer.css";
import Loader from "../../loader";

class HeadlinesContainer extends Component {
  constructor(props) {
    super(props);

    this.state = {
      apiResponse: this.props.dataResponse,
      loading: true,
    };
  }
  // sendData = (data) => {
  //   console.log(this.props);
  //   this.props.parentCallback(data);
  // };
  // callAPI() {
  //   fetch("http://localhost:9000/covidHeadlines")
  //     .then((res) => res.json())
  //     .then((res) => {
  //       this.setState({ apiResponse: res, loading: false });
  //       // this.sendData(false);
  //     });
  // }

  // componentWillMount() {
  //   this.callAPI();
  // }

  render() {
    let d = this.state.apiResponse;
    console.log(d);

    return (
      <div>
        <h4 className="descriptionBoxHeading">One-shot Status</h4>
        {d.Country ? (
          <ul className="description">
            <li>
              Country wise <strong>{d.Country}</strong> has the highest record
              of newly confirmed cases
            </li>
          </ul>
        ) : (
          ""
        )}
        <ul className="description">
          <li>
            State wise <strong>{d.Province}</strong> has the highest record of{" "}
            cases
          </li>
        </ul>

        {/* <ul className="description">
          <li>{this.props.dataFromParent}</li>
        </ul> */}
      </div>
    );
  }
}

export default HeadlinesContainer;
