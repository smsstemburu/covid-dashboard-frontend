import React, { Component } from "react";
import "../../Styles/SpecificStateContainer.css";

import { BarChart, Bar, XAxis, YAxis, CartesianGrid, Legend } from "recharts";

export default class SpecificStateContainer extends Component {
  constructor(props) {
    super(props);

    this.state = {
      apiResponse: [],
      countries: [],
      states: [],
      countryValue: "India",
      stateValue: "Telangana",
      result: [
        {
          Province: "Odisha",
          Confirmed: 1040230,
          Deaths: 8322,
          Recovered: 0,
          Active: 1031908,
        },
      ],
      countryResult: [
        {
          Country: "India",
          NewConfirmed: 16156,
          TotalConfirmed: 34231809,
          NewDeaths: 733,
          TotalDeaths: 456386,
          NewRecovered: 0,
          TotalRecovered: 0,
        },
      ],
      loading: true,
    };
  }
  static demoUrl =
    "https://codesandbox.io/s/pie-chart-with-customized-label-dlhhj";

  empty(d) {
    //empty your array
    this.setState({ result: [] });
    this.setState((prevState) => ({
      result: [d, ...prevState.result],
    }));
  }

  callAPI() {
    fetch("http://localhost:9000/covidHeadlines")
      .then((res) => res.json())
      .then((res) => this.setState({ apiResponse: res, loading: false }));
  }

  callSpecificStateAPI() {
    fetch("http://localhost:9000/covidAllStateStatus")
      .then((res) => res.json())
      .then((res) => this.setState({ states: res }));
  }

  componentWillMount() {
    this.callAPI();

    this.callSpecificStateAPI();
  }

  click = async (stateName) => {
    this.setState({ stateValue: stateName });
    // const stateValue = this.state.stateValue;
    await fetch("http://localhost:9000/covidSpecificStateStatus", {
      method: "POST",
      headers: { "Content-Type": "application/json" },
      body: JSON.stringify({
        Province: stateName,
      }),
    })
      .then((res) => res.json())
      //   .then((d) => this.setState({result: d}))
      .then((d) => this.empty(d));
    return "success";
  };

  render() {
    console.log(this.state.result);
    const { states } = this.state;
    let statesList =
      states.length > 0 &&
      states.map((item, i) => {
        return (
          <option key={i} value={item.id}>
            {item.Province}
          </option>
        );
      }, this);
    return (
      <div>
        <h4 className="descriptionBoxHeading">
          <select
            className="specificCountryDropdown"
            onChange={(e) => this.click(e.target.value)}
          >
            {statesList}
          </select>
        </h4>

        <div style={{ position: "relative" }}>
          <BarChart
            //   width={((this.state.width-60)>(50*this.state.filteredData.length*this.state.selectedMeasures.length))?(this.state.width-60):(50*this.state.filteredData.length*this.state.selectedMeasures.length)}

            data={this.state.result}
            width={400}
            height={200}
            margin={{ top: 5, right: 30, left: 20, bottom: 5 }}
            style={{
              overflowX: "auto",
              overflowY: "hidden",
              maxHeight: 186,
              maxWidth: 700,

              position: "relative",
              zIndex: "0",
            }}
            barCategoryGap={10}
          >
            <YAxis />
            <XAxis
              minTickGap={1}
              dataKey="Province"
              style={{ fontSize: "10px", color: "antiquewhite" }}
            />

            <Legend />
            <Bar dataKey="Confirmed" fill="#EF7B45" />

            <Bar dataKey="Active" fill="antiquewhite" />

            <Bar dataKey="Deaths" fill="#F5C0C0" />
          </BarChart>
        </div>
      </div>
    );
  }
}
