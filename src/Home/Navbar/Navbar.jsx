import React, { Component } from "react";
import ThemeSwitcher from "react-theme-switcher";
import "../../Styles/Navbar.css";
import ReactToPdf from "react-to-pdf";
import jsPDF from "jspdf";
import domtoimage from "dom-to-image";

class NavBar extends Component {
  // timeSetter = (forPDF, thisPDF, i, titleWidth, node, len) => {
  //   return setTimeout(
  //     function (i) {
  //       let elements = document.getElementsByTagName("tspan");
  //       for (let ele of elements) {
  //         ele.style.fontSize = "11px";
  //       }
  //       console.log("Printing nodes");
  //       console.log(node.childNodes[i]);

  //       if (node.childNodes[i - 1])
  //         domtoimage
  //           .toPng(node.childNodes[i - 1], { bgcolor: "white" })
  //           .then((dataUrl) => {
  //             //console.log(document.getElementById("App").childNodes[i].childNodes[0]!="+");

  //             document.getElementById("App").style.height = "0px";

  //             document.getElementById("App").style.height = "571.58px";

  //             if (i != 0) {
  //               //   console.log("onrendered");
  //               //   console.log(forPDF.childNodes[i-1].getAttribute("id"));
  //               thisPDF.addImage(dataUrl, "PNG", 0, 0);
  //               if (parseInt(i + 1) === len + 1) {
  //                 // console.log(forPDF.childNodes[i-1].childNodes[0])
  //                 // console.log("saved");

  //                 document.getElementById("App").style.height = "0px";

  //                 document.getElementById("App").style.height = "800.58px";

  //                 thisPDF.save("report.pdf");
  //                 if (localStorage.getItem("IsCreator") == "true") {
  //                   if (
  //                     document.getElementById("App").childNodes[
  //                       document.getElementById("App").childNodes.length - 2
  //                     ]
  //                   ) {
  //                     //console.log((document.getElementById("App").childNodes[document.getElementById("App").childNodes.length-2]));
  //                     document
  //                       .getElementById("App")
  //                       .childNodes[
  //                         document.getElementById("App").childNodes.length - 2
  //                       ].childNodes[2].click();
  //                   }
  //                 } else {
  //                   //console.log(document.getElementById(document.getElementById("App").getAttribute("activePage")));
  //                   //console.log(document.getElementById("App").childNodes[document.getElementById("App").childNodes.length-2]);

  //                   document
  //                     .getElementById("App")

  //                     .remove();

  //                   // document.getElementById(document.getElementById("App").getAttribute("activePage")).style.height="0px";

  //                   // document.getElementById(document.getElementById("App").getAttribute("activePage")).style.height="571.58px";
  //                 }
  //               } else {
  //                 thisPDF.addPage();
  //                 //console.log("page added");
  //                 //console.log(i+1,len);
  //               }
  //             }
  //           });
  //     },
  //     1000 * (i * 4),
  //     i
  //   );
  // };

  // testPdfNew = () => {
  //   console.log("testing pdf");
  //   document.getElementById("App").childNodes[0].click();
  //   var forPDF = document.getElementById("App");
  //   var len = forPDF.childNodes.length;
  //   var newWidth;
  //   var titleWidth;
  //   var newHeight = window.innerHeight - 330;
  //   newWidth = document.getElementById("App").offsetWidth - 285;
  //   titleWidth = "1146px";

  //   var thisPDF = new jsPDF("l", "px", [newWidth, newHeight]); //210mm wide and 297mm high
  //   console.log(document.getElementById("App").clientHeight);
  //   console.log(newWidth);
  //   console.log(len);
  //   // var options={};
  //   // options.style = {
  //   //     'left':0,
  //   //     'right':0
  //   //   }
  //   //console.log(document.getElementById("App").childNodes[0]);
  //   document.getElementById("App").childNodes[0].click();
  //   document.getElementById("App").style.height = "571.58px";
  //   // setTimeout(function(){

  //   // },1000)
  //   var node = document.getElementById("App");
  //   for (var i = 0; i < forPDF.childNodes.length + 1; i++) {
  //     //document.getElementById(document.getElementById("App").getAttribute("activePage")).style.height="571.58px";
  //     this.timeSetter(forPDF, thisPDF, i, titleWidth, node, len);
  //   }

  //   // console.log(document.getElementById(document.getElementById("App").getAttribute("activePage")));
  //   // document.getElementById(document.getElementById("App").getAttribute("activePage")).style.height="0px";
  // };

  render() {
    const ref = React.createRef();
    return (
      <div>
        <nav class="navbar navbar-inverse">
          <div class="container-fluid">
            <div class="navbar-header">
              <a
                class="navbar-brand"
                href="#"
                style={{ color: "antiquewhite", fontWeight: "bold" }}
              >
                Covid Dashboard
              </a>
            </div>

            <ul class="nav navbar-nav navbar-right">
              <li>
                <a href="#">
                  <span>
                    <ThemeSwitcher />
                  </span>{" "}
                </a>
              </li>
              <li>
                <a href="#">
                  <span class="glyphicon glyphicon-export"></span> Export to PDF
                </a>

                {/* <div>
                    <ReactToPdf targetRef={ref} filename="div-blue.pdf">
                      {({ toPdf }) => (
                        <button onClick={toPdf}>Generate pdf</button>
                      )}
                    </ReactToPdf>
                    <div
                      style={{
                        width: window.innerWidth,
                        height: window.innerHeight,
                        background: "blue",
                      }}
                      ref={ref}
                    />
                  </div> */}
              </li>
              <li>
                <a href="#">
                  <span class="glyphicon glyphicon-share"></span> Share
                </a>
              </li>
            </ul>
          </div>
        </nav>
      </div>
    );
  }
}

export default NavBar;
