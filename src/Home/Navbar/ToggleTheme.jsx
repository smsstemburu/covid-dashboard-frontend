import React, { Component } from 'react';
import { Switch, useDarkreader } from 'react-darkreader';

class ToggleTheme extends Component{

 
 render(){
   
  return (
    <div className="App">
      <h1>React dark mode example - codecheef</h1>
      <Switch checked={isDark} onChange={toggle} />
    </div>
  );
 }
}

export default App;