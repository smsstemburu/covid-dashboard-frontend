import React from "react";

function ShowDetail() {
  return (
    <div
      className="loader center"
      style={{ marginTop: "15%", textAlign: "center" }}
    >
      <i className="fa fa-cog fa-spin" style={{ fontSize: "500%" }} />
    </div>
  );
}

export default ShowDetail;
